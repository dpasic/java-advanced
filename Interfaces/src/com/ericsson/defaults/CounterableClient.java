package com.ericsson.defaults;

public class CounterableClient {

    private static final String STRING = "last day";

    public static void main(String[] args) {
        Counterable generic = new GenericCounter();
        System.out.println(generic.count(STRING));

        Counterable vowels = new VowelsCounter();
        System.out.println(vowels.count(STRING));
    }

}
