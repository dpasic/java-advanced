package com.ericsson.defaults;

// an interface can consist of both default and non-default methods
public interface Counterable {

    // default implementation is provided
    // it can be overridden, but it is not mandatory
    default int count(String s) {
        return s.length();
    }
}
