package com.ericsson.defaults;

public class VowelsCounter implements Counterable {

    @Override
    public int count(String s) {
        int counter = 0;
        for (char ch : s.toCharArray()) {
            switch (Character.toLowerCase(ch)) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                counter++;
                break;
            }
        }
        return counter;
    }

}
