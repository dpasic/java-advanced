package com.ericsson.basic;

// an interface can extend N interfaces
public interface RemoteControllable extends Switchable {

    void mute();
}
