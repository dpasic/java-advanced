package com.ericsson.basic;

public class Light implements Switchable {

    @Override
    public void switchOn() {
        System.out.println("Light switches on");
    }

    @Override
    public void switchOff() {
        System.out.println("Light switches off");
    }
}
