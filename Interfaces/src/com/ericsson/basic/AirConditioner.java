package com.ericsson.basic;

public class AirConditioner implements Switchable {

    @Override
    public void switchOn() {
        System.out.println("AirConditioner switches on");
    }

    @Override
    public void switchOff() {
        System.out.println("AirConditioner switches off");
    }

}
