package com.ericsson.basic;

public interface Switchable {

    // by default is public and abstract
    void switchOn();

    void switchOff();
}
