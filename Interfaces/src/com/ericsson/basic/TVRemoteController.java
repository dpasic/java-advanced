package com.ericsson.basic;

public class TVRemoteController implements RemoteControllable {

    @Override
    public void mute() {
        System.out.println("TVRemoteController mutes");
    }

    @Override
    public void switchOn() {
        System.out.println("TVRemoteController switches on");
    }

    @Override
    public void switchOff() {
        System.out.println("TVRemoteController switches on");
    }
}
