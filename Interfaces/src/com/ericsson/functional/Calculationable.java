package com.ericsson.functional;

// an informative compiler annotation
// ensures that only one method will be provided
@FunctionalInterface
public interface Calculationable {

    // lambda can be used for a functional interface method
    double calc(double a, double b);
}
