package com.ericsson.functional;

public class CalculatorClient {

    private static final double A = 5;
    private static final double B = 3;

    public static void main(String[] args) {
        Calculationable addition = new Addition();
        calculate(A, B, addition);

        // Anonymous Type (implementation)
        //        Calculationable subtraction = new Calculationable() {
        //
        //            @Override
        //            public double calc(double a, double b) {
        //                return a - b;
        //            }
        //        };

        // lambda expression which implements calc() method
        // curly braces have to be used in case of multi-line body
        Calculationable subtraction = (a, b) -> {
            return a - b;
        };
        calculate(A, B, subtraction);

        // curly braces can be omitted in case where additional variables are not needed
        calculate(A, B, (a, b) -> a * b);
    }

    private static void calculate(double a, double b, Calculationable calculationable) {
        System.out.printf("Result: %.2f%n", calculationable.calc(a, b));
    }

}
