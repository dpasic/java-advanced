package com.ericsson.functional;

public class Addition implements Calculationable {

    @Override
    public double calc(double a, double b) {
        return a + b;
    }

}
